package me.emmano.dario.news

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.text.Html
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.news_article_row.view.*
import me.emmano.dario.R
import me.emmano.dario.bus.RxBus
import me.emmano.dario.bus.events.LaunchArticleActivityEvent
import me.emmano.dario.data.NewsArticle

class NewsArticleAdapter(var articles: List<NewsArticle>) : RecyclerView.Adapter<NewsArticleViewHolder>() {

  override fun onBindViewHolder(holder: NewsArticleViewHolder, position: Int) {
    val newsArticle = articles[position]
    val newsArticlesWithImages = newsArticle.thumbnails.filter {
      ("imagen".equals(it.type.type) || "video".equals(it.type.type) ) && !it.files.isEmpty()
    }
    if(!newsArticlesWithImages.isEmpty()) {
      val imageUrl = newsArticlesWithImages.first().files.filter { !it.url.isEmpty() && ("imagen".equals(it.type) || "snapshot".equals(it.type)) }.first().url
      if (!imageUrl.isEmpty()) {
        holder.rowView.news_article_row_image.visibility = View.VISIBLE
        Glide.with(holder.rowView.news_article_row_image.context).load(imageUrl).into(
            holder.rowView.news_article_row_image)
      } else {
        holder.rowView.news_article_row_image.visibility = View.GONE
      }
    }
    holder.rowView.news_article_row_title.text = newsArticle.title
    holder.rowView.news_article_row_caption.text = Html.fromHtml(newsArticle.caption.plus("<br>"))
    holder.rowView.tag = newsArticle.id
    holder.rowView.setOnClickListener {
      RxBus.create().send(LaunchArticleActivityEvent(it.tag as String, holder.rowView))
    }
  }

  override fun getItemCount(): Int = articles.size

  override fun onCreateViewHolder(parent: ViewGroup?,
      viewType: Int): NewsArticleViewHolder = NewsArticleViewHolder(
      NewsArticleRowView(parent?.context!!))
}

class NewsArticleViewHolder(val rowView: NewsArticleRowView) : RecyclerView.ViewHolder(rowView)

class NewsArticleRowView(val viewContext: Context) : RelativeLayout(viewContext) {

  init {
    View.inflate(viewContext, R.layout.news_article_row, this)
  }

}
