package me.emmano.dario.news

import android.util.Log
import me.emmano.dario.data.Category
import me.emmano.dario.data.Repository
import me.emmano.dario.presenter.Presenter
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class NewsActivityPresenter(val categoryItems: NewsCategoryView, val categoryRepository: Repository<Category>) : Presenter {

  val categoryObservable: Observable<List<Category>> by lazy { categoryRepository.loadList() }
  val drawerItemsSubscriber: Subscriber<List<Category>> = object : Subscriber<List<Category>>() {
    override fun onCompleted() {

    }

    override fun onError(e: Throwable?) {
      Log.e("asdasdas", e.toString())
    }

    override fun onNext(drawerItems: List<Category>) {
      categoryItems.showDrawerCategories(drawerItems)
    }

  }

  fun loadDrawerItems() {
    categoryObservable
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(drawerItemsSubscriber)
  }

  override fun onPause() {
    drawerItemsSubscriber.unsubscribe()
  }

  interface NewsCategoryView {
    fun showDrawerCategories(drawerCategories: List<Category>)
  }
}