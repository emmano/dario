package me.emmano.dario.news

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import kotlinx.android.synthetic.main.news_fragment.*
import me.emmano.dario.R
import me.emmano.dario.data.NewsArticle
import me.emmano.dario.data.NewsArticleRepository

class NewsFragment : Fragment(), NewsPresenter.NewsArticlesView {

  val presenter: NewsPresenter by lazy {
    NewsPresenter(arguments.getInt("portalId"), arguments.getString("categoryId"), this,
        NewsArticleRepository())
  }

  override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?,
      savedInstanceState: Bundle?): View? {
    return inflater?.inflate(R.layout.news_fragment, container, false)
  }

  override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
    super.onViewCreated(view, savedInstanceState)
    val layoutManager = LinearLayoutManager(activity)
    recycler_view.layoutManager = layoutManager
    recycler_view.setHasFixedSize(true)
    swipe_to_refresh.viewTreeObserver.addOnGlobalLayoutListener(
        object : ViewTreeObserver.OnGlobalLayoutListener {
          override fun onGlobalLayout() {
            swipe_to_refresh.viewTreeObserver.removeOnGlobalLayoutListener(this)
            swipe_to_refresh.isRefreshing = true
            presenter.loadNews()
          }

        })
    swipe_to_refresh.setOnRefreshListener {
        presenter.loadNews();
        swipe_to_refresh.isRefreshing = true
    }
  }

  override fun onPause() {
    presenter.onPause()
    super.onPause()
  }

  override fun showArticleList(newsArticles: List<NewsArticle>) {
    swipe_to_refresh.isRefreshing = false
    val filteredList = newsArticles.filter {
      !it.thumbnails.isEmpty() && !"script".equals(it.thumbnails.first().type.type)
    }
    recycler_view.adapter = NewsArticleAdapter(filteredList)
  }
}