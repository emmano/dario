package me.emmano.dario.news

import android.util.Log
import me.emmano.dario.data.NewsArticle
import me.emmano.dario.data.NewsArticleRepository
import me.emmano.dario.data.Repository
import me.emmano.dario.presenter.Presenter
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class NewsPresenter(val portalId: Int, val categoryId: String, val newsArticlesView: NewsArticlesView, val newsRepository: Repository<NewsArticle>) : Presenter {

  val newsArticleObservable: Observable<List<NewsArticle>> by lazy { newsRepository.loadList(categoryId, portalId) }
  lateinit var newsArticleSubscriber: Subscriber<List<NewsArticle>>

  fun loadNews() {
    newsArticleSubscriber = object : Subscriber<List<NewsArticle>>() {
      override fun onCompleted() {

      }

      override fun onError(e: Throwable?) {
        Log.e("asdasdas", e.toString())
      }

      override fun onNext(newsArticles: List<NewsArticle>) {
        newsArticlesView.showArticleList(newsArticles)
      }

    }
    newsArticleObservable
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(newsArticleSubscriber)
  }

  override fun onPause() {
    newsArticleSubscriber.unsubscribe()
  }

  interface NewsArticlesView {
    fun showArticleList(newsArticles: List<NewsArticle>)
  }
}