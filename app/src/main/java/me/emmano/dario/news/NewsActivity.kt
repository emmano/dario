package me.emmano.dario.news

import android.content.res.Configuration
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import com.appsee.Appsee
import kotlinx.android.synthetic.main.news_activity_layout.*
import me.emmano.dario.R
import me.emmano.dario.bus.RxBus
import me.emmano.dario.bus.events.LaunchArticleActivityEvent
import me.emmano.dario.data.Category
import me.emmano.dario.data.CategoryRepository
import me.emmano.dario.navigator.toActivity
import me.emmano.dario.newsarticle.NewsArticleActivity
import rx.Observable
import rx.Subscription
import java.util.*

class NewsActivity : AppCompatActivity(), NewsActivityPresenter.NewsCategoryView {
  val presenter: NewsActivityPresenter by lazy { NewsActivityPresenter(this, CategoryRepository()) }
  lateinit var subscription: Subscription
  lateinit var menuList: ArrayList<MenuItem>
  var currentlySelectedMenuItem: MenuItem? = null
  val bus: Observable<Any> by lazy { RxBus.create().toObservable() }
  val toggle: ActionBarDrawerToggle by lazy {
    ActionBarDrawerToggle(this, drawer, toolbar, R.string.drawer_opened, R.string.drawer_closed)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.news_activity_layout)
    Appsee.start(getString(R.string.com_appsee_apikey))
    setSupportActionBar(toolbar)
    presenter.loadDrawerItems()
    navigation_view.setNavigationItemSelectedListener {
      if (currentlySelectedMenuItem != null) {
        currentlySelectedMenuItem?.isChecked = false;
      }
      currentlySelectedMenuItem = it;
      currentlySelectedMenuItem?.isChecked = true
      pager.currentItem = menuList.indexOf(it)
      it.isChecked = true
      drawer.closeDrawers()
      true
    }
  }

  override fun onPostCreate(savedInstanceState: Bundle?) {
    super.onPostCreate(savedInstanceState)
    toggle.syncState()
  }

  override fun onConfigurationChanged(newConfig: Configuration?) {
    super.onConfigurationChanged(newConfig)
    toggle.onConfigurationChanged(newConfig);
  }

  override fun onResume() {
    super.onResume()
    subscription = bus.subscribe { event ->
      toActivity(
          NewsArticleActivity::class.java,
          (event as LaunchArticleActivityEvent).rootView, { NewsArticleActivity.ARTICLE_ID to event.articleId })
    }
  }

  override fun onPause() {
    presenter.onPause()
    subscription.unsubscribe()
    super.onPause()
  }

  override fun showDrawerCategories(drawerCategories: List<Category>) {
    setupDrawerCategories(drawerCategories)
    setUpTabNavigation(drawerCategories)
  }

  private fun setUpTabNavigation(drawerCategories: List<Category>) {
    pager.adapter = ViewPagerAdapter(drawerCategories, supportFragmentManager)
    tabs.setupWithViewPager(pager)
    pager.addOnPageChangeListener(object : TabLayout.TabLayoutOnPageChangeListener(tabs) {
      override fun onPageSelected(position: Int) {
        super.onPageSelected(position)
        if (currentlySelectedMenuItem != null) {
          currentlySelectedMenuItem?.isChecked = false
        }
        currentlySelectedMenuItem = menuList[position]
        currentlySelectedMenuItem?.isChecked = true
      }
    })
  }

  private fun setupDrawerCategories(
      drawerCategories: List<Category>) {
    val menu = navigation_view.menu
    drawerCategories.forEach {
      menu.add(R.id.navigation_drawer_group, it.id, Menu.NONE, it.description)
    }
    val menuSize = navigation_view.menu.size()
    menuList = ArrayList<MenuItem>(menuSize)
    for (i in 0..menuSize - 1) {
      menuList.add(navigation_view.menu.getItem(i))
    }
    currentlySelectedMenuItem = menuList.first()
    currentlySelectedMenuItem?.isChecked = true
  }

}

class ViewPagerAdapter(val categoryList: List<Category>, val supportFragmentManager: FragmentManager) : FragmentStatePagerAdapter(
    supportFragmentManager) {
  override fun getItem(position: Int): Fragment? {
    val fragment = NewsFragment()
    val bundle = Bundle()
    bundle.putString("categoryId", categoryList[position].name)
    bundle.putInt("portalId", categoryList[position].portalId)
    fragment.arguments = bundle
    return fragment
  }

  override fun getCount(): Int = categoryList.size

  override fun getPageTitle(position: Int): CharSequence? = categoryList[position].description

}
