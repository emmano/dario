package me.emmano.dario.presenter

interface Presenter {
  fun onPause()
}