package me.emmano.dario.data

import com.google.gson.GsonBuilder
import com.squareup.okhttp.OkHttpClient
import com.squareup.okhttp.logging.HttpLoggingInterceptor
import retrofit.GsonConverterFactory
import retrofit.Retrofit
import retrofit.RxJavaCallAdapterFactory
import rx.Observable

abstract class Repository<T> {
  protected val retrofit: Retrofit by lazy {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

    val client = OkHttpClient()
    client.interceptors().add(httpLoggingInterceptor)

    Retrofit.Builder()
        .baseUrl("http://api.infobae.com/1.0/public/")
        .addConverterFactory(GsonConverterFactory.create(
            GsonBuilder().registerTypeAdapter(NewsArticle.Body::class.java,
                NewsArticle.Body.BodyDeserializer()).create()))
        .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
        .client(client)
        .build()
  }
  abstract fun loadList(categoryId: String? = null, portalId: Int? = null) : Observable<List<T>>
  abstract fun loadSingle(itemId: String) : Observable<T>
}