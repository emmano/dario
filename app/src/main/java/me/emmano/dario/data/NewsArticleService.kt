package me.emmano.dario.data

import retrofit.http.GET
import retrofit.http.Path
import retrofit.http.Query
import rx.Observable

interface NewsArticleService {

  @GET("articles/page?channel=mobile&page=1&size=40&mediaObjectSizeIds=5")
  fun loadArticlesForCategory(@Query("name") categoryId: String, @Query("portal") portal: Int): Observable<NewsArticleWrapper>

  @GET("articles/{articleId}?formattedBody=true&mediaObjectSizeIds=5%2C1%2C2")
  fun loadArticle(@Path("articleId") articleId: String) : Observable<NewsArticle>
}