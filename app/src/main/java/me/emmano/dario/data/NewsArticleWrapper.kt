package me.emmano.dario.data

data class NewsArticleWrapper(val newsArticles: List<NewsArticle>)