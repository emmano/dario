package me.emmano.dario.data

/*
*
* "id": 1,
	"parentId": 0,
	"webPageId": 40,
	"portalId": 162,
	"order": 0,
	"isMainPage": true,
	"description": "Ahora",
	"name": "home",
	"category": []
*
*
* */
data class Category(val id: Int, val parentId: Int, val webPageId: Int, val portalId: Int, val order: Int, val isMainPage: Boolean, val description: String, val name: String, val categories: List<Category>) {
}