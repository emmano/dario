package me.emmano.dario.data

import rx.Observable

class NewsArticleRepository() : Repository<NewsArticle>() {
  val newsService: NewsArticleService by lazy { retrofit.create(NewsArticleService::class.java) }

  override fun loadList(categoryId: String?, portalId: Int?): Observable<List<NewsArticle>> = newsService.loadArticlesForCategory(categoryId!!, portalId!!).map { it.newsArticles }

  override fun loadSingle(itemId: String): Observable<NewsArticle> = newsService.loadArticle(itemId)
  }

