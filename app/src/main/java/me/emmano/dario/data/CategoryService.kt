package me.emmano.dario.data

import retrofit.http.GET
import rx.Observable

interface CategoryService {
  @GET("page/mobile/menu?portal=162")
  fun loadCategories() : Observable<List<Category>>
}