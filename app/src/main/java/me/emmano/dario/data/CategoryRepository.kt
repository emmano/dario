package me.emmano.dario.data

import rx.Observable

class CategoryRepository : Repository<Category>() {

  override fun loadList(categoryId: String?, portalId: Int?): Observable<List<Category>> = retrofit.create(CategoryService::class.java).loadCategories()

  override fun loadSingle(itemId: String): Observable<Category> {
    throw UnsupportedOperationException()
  }
}