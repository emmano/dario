package me.emmano.dario.data

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import java.lang.reflect.Type


/* NEWS ARTICLE

    "id":"1787722",
     "title":"Seis referentes del PJ responden: \u00bfElecciones o candidato \u00fanico? \u00bfBossio se equivoc\u00f3?",
     "homeTitle":"La crisis del PJ, seg\u00fan 6 referentes: \"No peleamos, nos estamos reproduciendo\"",
     "caption":"",
     "homeCaption":"Daniel Scioli, Antonio Cal\u00f3, Guillermo Moreno, Jorge Capitanich, H\u00e9ctor Recalde y Fernando Espinoza respondieron las preguntas de <b>Infobae<\/b> luego de una jornada de tensiones y decisiones pol\u00edticas",
     "shortTitle":"",
     "sections":[  ],
     "thumbnails":[  ],
     "mediaObjectCount":"1",
     "shortUrl":"http:\/\/www.infobae.com\/c1787722",
     "categories":[  ]

*/
data class NewsArticle(val id: String, val title: String, val homeTitle: String, val caption: String,
    val homeCaption: String, val shortTitle: String, val sections: List<String>, val thumbnails: List<Thumbnails>,
    val mediaObjectCount: Int, val shortUrl: String, val categories: List<Category>, val body: List<Body>, val mediaObjects: List<MediaObject>) {

//  class Section()

  /* THUMBNAIL

      "id":2167541,
      "portalId":166,
      "stateId":1,
      "caption":"",
      "copyright":"",
      "publicationDate":"04022016",
      "type":{  },
      "files":[  ]
  */
  data class Thumbnails(val id: Int, val portalId: Int, val stateId: Int, val caption: String, val copyRight: String,
      val publicationDate: String, val type: Type, val files: List<File>) {

    data class Type(val id: Int, val name: String, val type: String)

    data class File(val url: String, val width: Int, val height: Int, val type: String)

  }

  data class Body(val type: String, val content: Content) {

    class BodyDeserializer : JsonDeserializer<Body> {
      override fun deserialize(json: JsonElement?, typeOfT: Type?,
          context: JsonDeserializationContext?): Body? {
        val jsonObject = json?.asJsonObject
        var body: Body?
        if ("external".equals(jsonObject?.get("type")?.asString!!)) {
          body = Body(jsonObject?.get("type")?.asString!!,
              Content(jsonObject?.get("content")?.asString!!, "", "", ""))
        } else {
          body = Gson().fromJson(json, Body::class.java)
        }

        return body
      }

    }

    data class Content(var text: String, val url: String, val caption: String, val copyRight: String)

  }

  data class Category(val id: Int, val name: String)

  data class MediaObject(val files: List<Thumbnails.File>)

}





