package me.emmano.dario.bus.events

import me.emmano.dario.news.NewsArticleRowView

class LaunchArticleActivityEvent(val articleId: String, val rootView: NewsArticleRowView)