package me.emmano.dario.bus

import rx.Observable
import rx.subjects.PublishSubject
import rx.subjects.SerializedSubject
import rx.subjects.Subject
import kotlin.reflect.jvm.internal.impl.javax.inject.Singleton

@Singleton
class RxBus {

  private val bus: Subject<Any, Any> = SerializedSubject<Any, Any>(PublishSubject.create())

  companion object {
    private val rxBus: RxBus by lazy { RxBus() }
    fun create(): RxBus {
      return rxBus
    }

  }

  fun send(target: Any) {
    bus.onNext(target)
  }

  fun toObservable(): Observable<Any> = bus
}