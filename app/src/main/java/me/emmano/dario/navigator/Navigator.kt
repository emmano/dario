package me.emmano.dario.navigator

import android.content.Intent
import android.support.v4.app.ActivityOptionsCompat
import android.support.v7.app.AppCompatActivity
import android.view.View
import kotlinx.android.synthetic.main.news_article_row.view.*
import me.emmano.dario.news.NewsArticleRowView

fun <T : AppCompatActivity> AppCompatActivity.toActivity(to: Class<T>,
    rootSharedView: NewsArticleRowView,
    intentExtras: () -> Pair<String, String>) {
  val pair = intentExtras()
  val intent = Intent(this, to)
  intent.putExtra(pair.first, pair.second)
  val transitionPair1 = android.support.v4.util.Pair<View, String>(rootSharedView.news_article_row_image, "articleImage")
  val transitionPair2 = android.support.v4.util.Pair<View, String>(rootSharedView.news_article_row_title, "articleTitle")
  val transitionPair3 = android.support.v4.util.Pair<View, String>(rootSharedView.news_article_row_caption, "articleCaption")
  val options = ActivityOptionsCompat.
      makeSceneTransitionAnimation(this, transitionPair1, transitionPair2, transitionPair3)
  startActivity(intent, options.toBundle());

}