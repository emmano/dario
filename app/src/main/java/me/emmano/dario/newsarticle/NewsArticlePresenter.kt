package me.emmano.dario.newsarticle

import android.util.Log
import me.emmano.dario.data.NewsArticle
import me.emmano.dario.data.Repository
import me.emmano.dario.presenter.Presenter
import rx.Observable
import rx.Subscriber
import rx.android.schedulers.AndroidSchedulers
import rx.schedulers.Schedulers

class NewsArticlePresenter(val articleId: String, val newsArticleView: NewsArticlePresenter.NewsArticleView, val newsRepository: Repository<NewsArticle>) : Presenter {

  override fun onPause() {
    newsArticleSubscriber.unsubscribe();
  }

  val newsArticleObservable: Observable<NewsArticle> by lazy {
    newsRepository.loadSingle(articleId)
  }
  val newsArticleSubscriber: Subscriber<NewsArticle> = object : Subscriber<NewsArticle>() {
    override fun onCompleted() {

    }

    override fun onError(e: Throwable?) {
      Log.e("asdaswdas", e.toString())
    }

    override fun onNext(newsArticle: NewsArticle) {
      newsArticleView.showArticle(newsArticle)
    }

  }

  fun loadArticle() {
    newsArticleObservable
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(newsArticleSubscriber)
  }

  interface NewsArticleView {
    fun showArticle(newsArticle: NewsArticle)
  }

}