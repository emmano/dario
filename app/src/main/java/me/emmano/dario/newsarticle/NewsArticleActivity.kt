package me.emmano.dario.newsarticle

import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.graphics.Palette
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.TypedValue
import android.view.MenuItem
import android.view.View
import android.widget.LinearLayout
import android.widget.MediaController
import android.widget.VideoView
import com.appsee.Appsee
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.BitmapImageViewTarget
import kotlinx.android.synthetic.main.news_article_activity.*
import kotlinx.android.synthetic.main.news_article_row.view.*
import me.emmano.dario.R
import me.emmano.dario.data.NewsArticle
import me.emmano.dario.data.NewsArticleRepository

class NewsArticleActivity : AppCompatActivity(), NewsArticlePresenter.NewsArticleView {
  companion object {
    val ARTICLE_ID: String = "articleID"
  }

  val presenter: NewsArticlePresenter by lazy {
    NewsArticlePresenter(intent.getStringExtra(ARTICLE_ID), this, NewsArticleRepository())
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.news_article_activity)
    setSupportActionBar(toolbar)
    supportActionBar?.setDisplayShowHomeEnabled(true);
    supportActionBar?.setDisplayHomeAsUpEnabled(true);
    Appsee.start(getString(R.string.com_appsee_apikey))
  }

  override fun onResume() {
    super.onResume()
    presenter.loadArticle()
  }

  override fun onPause() {
    presenter.onPause()
    super.onPause()
  }

  override fun onOptionsItemSelected(item: MenuItem?): Boolean {
    when (item?.itemId) {
      android.R.id.home -> {
        supportFinishAfterTransition();
        return true
      }
    }

    return super.onOptionsItemSelected(item)

  }

  override fun showArticle(newsArticle: NewsArticle) {
    //TODO move this to separate class
    news_article_activity_title.text = newsArticle.title

    news_article_activity_caption.text = Html.fromHtml(
        newsArticle.caption.replace("\\n", "<br>").plus("<br>"))
    news_article_activity_caption.movementMethod = LinkMovementMethod.getInstance()

    val headerImage = newsArticle.mediaObjects.flatMap { it.files }.filter { ("imagen".equals(it.type) || "snapshot".equals(it.type)) && it.height > 400 }.first().url

    Glide.with(news_article_activity_header_image.context).load(headerImage).asBitmap().into(
        object : BitmapImageViewTarget(news_article_activity_header_image) {
          override fun setResource(resource: Bitmap?) {
            super.setResource(resource)
            news_article_activity_header_image.setImageBitmap(resource)
            Palette.from(resource).generate {
              val darkVibrantColor = it.getVibrantColor(Color.parseColor("#FF9E9E9E"))
              news_article_activity_coordinator_layout.setContentScrimColor(darkVibrantColor)
            }
          }
        })

    newsArticle.body.forEach {
      when (it.type) {
        "text" -> {
          val bodyTextView = AppCompatTextView(this)
          bodyTextView.movementMethod = LinkMovementMethod.getInstance()
          bodyTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 16f)
          bodyTextView.text = Html.fromHtml(it.content.text.replace("\\n", "<br><br>"))
          bodyTextView.setTextColor(Color.parseColor("#FF212121"))
          news_article_activity_body.addView(bodyTextView)

        }
        "imagen" -> {
          val imageView = AppCompatImageView(this)
          Glide.with(imageView.context).load(it.content.url).into(imageView)
          news_article_activity_body.addView(imageView)
        }
      }
    }
    newsArticle.mediaObjects.forEach {
      it.files.forEach {

        when (it.type) {
          "video" -> {
            val videoView = VideoView(this)
            videoView.setVideoURI(Uri.parse(it.url));
            val mediaController = MediaController(this)
            mediaController.setMediaPlayer(videoView)
            videoView.setMediaController(mediaController)
            videoView.seekTo(100)
            videoView.requestFocus()
            val layoutParams = LinearLayout.LayoutParams(news_article_activity_body.width,
                news_article_activity_body.width)
            layoutParams.bottomMargin = resources.getDimensionPixelSize(
                R.dimen.activity_horizontal_margin) * 2
            videoView.layoutParams = layoutParams
            news_article_activity_body.addView(videoView)
            mediaController.setAnchorView(videoView)
          }
          "imagen" -> {
            if(it.height > 400) {
              val imageView = AppCompatImageView(this)
              Glide.with(imageView.context).load(it.url).into(imageView)
              news_article_activity_body.addView(imageView)
            }
          }
        }

      }
    }
    news_article_activity_scroll_view.scrollTo(0, 0)
  }
}
