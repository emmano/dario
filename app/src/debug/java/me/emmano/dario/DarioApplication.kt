package me.emmano.dario

import android.app.Application
import com.crashlytics.android.Crashlytics
import com.squareup.leakcanary.LeakCanary
import io.fabric.sdk.android.Fabric

class DarioApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    LeakCanary.install(this)
    Fabric.with(this, Crashlytics());
  }
}