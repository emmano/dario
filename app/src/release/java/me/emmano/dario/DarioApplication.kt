package me.emmano.dario

import android.app.Application
import com.crashlytics.android.Crashlytics
import io.fabric.sdk.android.Fabric

class DarioApplication : Application() {

  override fun onCreate() {
    super.onCreate()
    Fabric.with(this, Crashlytics());
  }
}